# joinbrands_test
Hugo Casildo Test
## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Linkedin Profile
```
[LinkedIn] (https://www.linkedin.com/in/hugo-casildo-pino/)
```

### GitLab 
```
[GitLAb](https://gitlab.com/hugo.casildo/)
```
### GitLab Project
```
[JoinBrands test]: (https://gitlab.com/hugo.casildo/joinbrands-test)
```

### URL Project
```
[URL]: (https://joinbrands-test.netlify.app)
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
