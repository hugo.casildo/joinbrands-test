import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import HomeView from '@/components/views/HomeView.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/sing-up',
    name: 'sing-up',
    component: () => import(/* webpackChunkName: "sing-up" */'@/components/views/SingUp.vue')
  },
  {
    path: '/sing-in',
    name: 'sing-in',
    component: () => import(/* webpackChunkName: "sing-in" */'@/components/views/SingIn.vue')
  },
  {
    path: '/lost-password',
    name: 'lost-password',
    component: () => import(/* webpackChunkName: "lost-password" */'@/components/views/LostPassword.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/profile',
    name: 'profile',
    component: () => import(/* webpackChunkName: "profile" */'@/components/views/ProfileView.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/change-password',
    name: 'change-password',
    component: () => import(/* webpackChunkName: "change-password" */'@/components/views/ChangePassword.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/sing-out',
    name: 'sing-out',
    component: () => import(/* webpackChunkName: "sing-out" */'@/components/views/SingOut.vue')
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to, from, next) => {
  if(to.meta.requiresAuth) {
    const userLoged = localStorage.getItem('userLoged')
    if (userLoged) {
      next()
    } else {
      next({ name: 'home'})
    }
  } else {
    next();
  }
})

export default router
